# Template for backend course
Для запуска бота:
1) Установить зависимости
```
    pip install -r requirements.txt
```
2) Создать файл .env с данными к БД и токеном для бота. Пример .env.example
3) Запустить миграции
```
    make migrate
```
4) Создать суперпользователя
```
    make createsuperuser
```
5) Запустить бота
```
    make bot
```
6) Запустить в отдельной консоли django
```
    make dev
```


