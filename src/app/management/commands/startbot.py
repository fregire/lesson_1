import os

import django
from django.core.management.base import BaseCommand, CommandError

from app.internal.bot import TelegramBot
from app.internal.transport.bot.handlers import me, set_phone, start, text_messages

django.setup()


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            bot = TelegramBot(os.environ.get("BOT_TOKEN"))
            bot.add_command("start", start)
            bot.add_command("set_phone", set_phone)
            bot.add_command("me", me)
            bot.add_txt_msg_handler(text_messages)
            bot.start()
        except KeyboardInterrupt:
            bot.stop()
