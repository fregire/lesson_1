import imp
import json
import re
from typing import Dict

from ..models.user import User

PHONE_PATTERN = r"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"


class UserService:
    def has_phone(self, id):
        user = User.objects.get(user_id=id)
        return user.phone is not None

    def add_or_update(self, user_data):
        User.objects.update_or_create(user_data)

    def get_user(self, **kargs):
        try:
            return User.objects.get(**kargs)
        except User.DoesNotExist:
            return None

    def serialize(self, user: User):
        user_dict = {"user_id": user.user_id, "username": user.username}
        if user.first_name:
            user_dict["first_name"] = user.first_name
        if user.last_name:
            user_dict["last_name"] = user.last_name
        if user.phone:
            user_dict["phone"] = user.phone

        return json.dumps(user_dict, ensure_ascii=False)

    @staticmethod
    def is_valid_phone(phone):
        return re.match(PHONE_PATTERN, phone) is not None


user_service = UserService()
