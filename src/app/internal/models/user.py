from tabnanny import verbose

from django.db import models
from django.forms import IntegerField


class User(models.Model):
    user_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    phone = models.CharField(max_length=30, null=True, blank=True)
    # Можно будет хранить доп инфу о команде в дальнейшем
    current_command = models.CharField(max_length=30, null=True, blank=True, editable=False)

    class Meta:
        verbose_name = "Пользователь TG"
        verbose_name_plural = "Пользователи TG"
