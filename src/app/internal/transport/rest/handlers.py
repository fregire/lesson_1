from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_GET

from app.internal.services.user_service import user_service


@require_GET
def me(request):
    user_id = request.GET.get("id", None)
    username = request.GET.get("username", None)
    user = None
    if not user_id and not username:
        return HttpResponse("Необходимо указать id или username пользователя в качестве параметра GET запроса")
    if user_id:
        user = user_service.get_user(user_id=user_id)
    else:
        user = user_service.get_user(username=username)

    if not user:
        return HttpResponseNotFound("Not found")

    return HttpResponse(user_service.serialize(user), content_type="application/json")
