from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import user_service


def _get_user_data(user_data):
    return {
        "username": user_data.username,
        "first_name": user_data.first_name,
        "last_name": user_data.last_name,
        "user_id": user_data.id,
    }


def start(update: Update, context: CallbackContext):
    user = _get_user_data(update.effective_user)
    user_service.add_or_update(user)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Твои данные успешно добавлены/обновлены")


def set_phone(update: Update, context: CallbackContext):
    update_fields = {"user_id": update.effective_user.id, "current_command": "set_phone"}
    user_service.add_or_update(update_fields)
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Введите номер телефона в формате 7XXXXXXXXXX \nНапример: 79221110500"
    )


def text_messages(update: Update, context: CallbackContext):
    user_id = update.effective_user.id
    user_info = user_service.get_user(user_id=user_id)
    if user_info.current_command == "set_phone":
        phone = update.message.text
        if user_service.is_valid_phone(phone):
            update_fields = {"user_id": user_id, "current_command": None, "phone": phone}
            user_service.add_or_update(update_fields)
            context.bot.send_message(chat_id=update.effective_chat.id, text="Номер телефона успешно добавлен")
        else:
            context.bot.send_message(
                chat_id=update.effective_chat.id, text="Номер телефона в неправильном формате. Введите снова"
            )


def _get_formatted_user_info(user):
    result = [f"Логин: {user.username}"]
    if user.first_name:
        result.append(f"Имя: {user.first_name}")
    if user.last_name:
        result.append(f"Фамилия: {user.last_name}")
    if user.phone:
        result.append(f"Телефон: {user.phone}")

    return "\n".join(result)


def me(update: Update, context: CallbackContext):
    user_id = update.effective_user.id
    user = user_service.get_user(user_id=user_id)

    if user.phone:
        context.bot.send_message(chat_id=update.effective_chat.id, text=_get_formatted_user_info(user))
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Необходимо указать телефон! Воспользуйтесь командой /set_phone"
        )
