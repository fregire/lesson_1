from django.contrib import admin


class UserAdmin(admin.ModelAdmin):
    list_display = ("username", "first_name", "last_name")
    readonly_fields = ("user_id", "username")
