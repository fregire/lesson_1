from telegram.ext import CommandHandler, Filters, MessageHandler, Updater


class TelegramBot:
    def __init__(self, token: str):
        self._updater = Updater(token=token)
        self._dispatcher = self._updater.dispatcher

    def add_command(self, command_name: str, callback):
        self._dispatcher.add_handler(CommandHandler(command_name, callback))

    def add_txt_msg_handler(self, handler):
        filters = Filters.text & (~Filters.command)
        self._dispatcher.add_handler(MessageHandler(filters, handler))

    def start(self):
        self._updater.start_polling()
        self._updater.idle()

    def stop(self):
        self._updater.stop()
