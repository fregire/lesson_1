from django.urls import path

from .transport.rest.handlers import me

urlpatterns = [path("me", me)]
